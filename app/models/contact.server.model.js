'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Article Schema
 */
var ContactSchema = new Schema({
    name: {
        first: {
            type: String,
            default: ''
        },
        last: {
            type: String,
            default: ''
        }
    },
    attention: {
        type: String,
        default: ''
    },
    address: {
        line1: {
            type: String,
            default: ''
        },
        line2: {
            type: String,
            default: ''
        },
        zipcode: {
            type: String,
            default: ''
        },
        city: {
            type: String,
            default: ''
        },
        country: {
            type: String,
            default: ''
        }
    },
    email: {
        type: String,
        default: ''
    },
    birthdate: {
        type: Date,
        default: ''
    },
    phone: {
        primary: {
            type: String,
            default: ''
        },
        secondary: {
            type: String,
            default: ''
        }
    },
    taxnumber: {
        type: String,
        default: ''
    },
    chamberOfCommerce: {
        type: String,
        default: ''
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: 'UserID has to be provided.'
    }
});

mongoose.model('Contact', ContactSchema);
