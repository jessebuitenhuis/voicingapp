'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact');

/**
 * Globals
 */
var user, contact;

/**
 * Unit tests
 */
describe('Article Model Unit Tests:', function() {
    beforeEach(function(done) {
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: 'username',
            password: 'password'
        });

        user.save(function() {
            contact = new Contact({
                name: {
                    first: 'First',
                    last: 'Last'
                },
                attention: 'Attention Name',
                address: {
                    line1: 'Street aderess 1',
                    line2: 'Street address 2',
                    zipcode: '34534 XB',
                    city: 'Utrecht',
                    country: 'Nederland'
                },
                email: 'firstname@email.com',
                birthdate: '1980-1-1',
                phone: {
                    primary: '06-12345678',
                    secondary: '06-12345678'
                },
                taxnumber: 'NL-12345678-ABCDEFG',
                chamberOfCommerce: 'NL-12345678-ABCDEFG',
                user: user
            });

            done();
        });
    });

    describe('Method Save', function() {
        it('should be able to save without problems', function(done) {
            return contact.save(function(err) {
                should.not.exist(err);
                done();
            });
        });

        it('should be able to show an error when try to save without user', function(done) {
            contact.user = '';

            return contact.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });

    afterEach(function(done) {
        Contact.remove().exec();
        User.remove().exec();
        done();
    });
});
