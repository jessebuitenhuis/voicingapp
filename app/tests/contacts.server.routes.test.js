'use strict';

var should = require('should'),
    request = require('supertest'),
    app = require('../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Contact = mongoose.model('Contact'),
    agent = request.agent(app);

/**
* Globals
*/
var credentials, user, contact;

/**
* Contact routes tests
*/
describe('Contact CRUD tests', function() {
    beforeEach(function(done) {
        // Create user credentials
        credentials = {
            username: 'username',
            password: 'password'
        };

        // Create a new user
        user = new User({
            firstName: 'Full',
            lastName: 'Name',
            displayName: 'Full Name',
            email: 'test@test.com',
            username: credentials.username,
            password: credentials.password,
            provider: 'local'
        });

        // Save a user to the test db and create new article
        user.save(function() {
            contact = new Contact({
                name: {
                    first: 'First',
                    last: 'Last'
                },
                attention: 'Attention Name',
                address: {
                    line1: 'Street aderess 1',
                    line2: 'Street address 2',
                    zipcode: '34534 XB',
                    city: 'Utrecht',
                    country: 'Nederland'
                },
                email: 'firstname@email.com',
                birthdate: '1980-1-1',
                phone: {
                    primary: '06-12345678',
                    secondary: '06-12345678'
                },
                taxnumber: 'NL-12345678-ABCDEFG',
                chamberOfCommerce: 'NL-12345678-ABCDEFG',
                user: user
            });

            done();
        });
    });

    it('should be able to save a contact if logged in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new contact
                agent.post('/contact')
                    .send(contact)
                    .expect(200)
                    .end(function(contactSaveErr, contactSaveRes) {
                        // Handle contact save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Get a list of contacts
                        agent.get('/contacts')
                            .end(function(contactsGetErr, contactsGetRes) {
                                // Handle contacts save error
                                if (contactsGetErr) done(contactsGetErr);

                                // Get articles list
                                var contacts = contactsGetRes.body;

                                // Set assertions
                                (contacts[0].user._id).should.equal(userId);
                                (contacts[0].attention).should.match('Attention Name');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to save a contact if not logged in', function(done) {
        agent.post('/contacts')
            .send(contact)
            .expect(401)
            .end(function(contactSaveErr, contactSaveRes) {
                // Call the assertion callback
                done(contactSaveErr);
            });
    });

    it('should be able to update a contact if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new article
                agent.post('/contacts')
                    .send(contact)
                    .expect(200)
                    .end(function(contactSaveErr, contactSaveRes) {
                        // Handle article save error
                        if (contactSaveErr) done(contactSaveErr);

                        // Update article title
                        contact.name.first = 'NewName';

                        // Update an existing article
                        agent.put('/contacts/' + contactSaveRes.body._id)
                            .send(contact)
                            .expect(200)
                            .end(function(contactUpdateErr, contactUpdateRes) {
                                // Handle article update error
                                if (contactUpdateErr) done(contactUpdateErr);

                                // Set assertions
                                (contactUpdateRes.body._id).should.equal(contactSaveRes.body._id);
                                (contactUpdateRes.body.name.first).should.match('NewName');

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });


    //HERE!
    it('should not be able to get a list of contacts if not signed in', function(done) {
        // Create new article model instance
        var contactObj = new Contact(contact);

        // Save the article
        contactObj.save(function() {
            // Request articles
            request(app).get('/contacts')
                .end(function(req, res) {
                    // Set assertion
                    res.body.should.be.an.Array.with.lengthOf(1);

                    // Call the assertion callback
                    done();
                });

        });
    });


    it('should be able to get a single article if not signed in', function(done) {
        // Create new article model instance
        var articleObj = new Article(article);

        // Save the article
        articleObj.save(function() {
            request(app).get('/articles/' + articleObj._id)
                .end(function(req, res) {
                    // Set assertion
                    res.body.should.be.an.Object.with.property('title', article.title);

                    // Call the assertion callback
                    done();
                });
        });
    });

    it('should return proper error for single article which doesnt exist, if not signed in', function(done) {
        request(app).get('/articles/test')
            .end(function(req, res) {
                // Set assertion
                res.body.should.be.an.Object.with.property('message', 'Article is invalid');

                // Call the assertion callback
                done();
            });
    });

    it('should be able to delete an article if signed in', function(done) {
        agent.post('/auth/signin')
            .send(credentials)
            .expect(200)
            .end(function(signinErr, signinRes) {
                // Handle signin error
                if (signinErr) done(signinErr);

                // Get the userId
                var userId = user.id;

                // Save a new article
                agent.post('/articles')
                    .send(article)
                    .expect(200)
                    .end(function(articleSaveErr, articleSaveRes) {
                        // Handle article save error
                        if (articleSaveErr) done(articleSaveErr);

                        // Delete an existing article
                        agent.delete('/articles/' + articleSaveRes.body._id)
                            .send(article)
                            .expect(200)
                            .end(function(articleDeleteErr, articleDeleteRes) {
                                // Handle article error error
                                if (articleDeleteErr) done(articleDeleteErr);

                                // Set assertions
                                (articleDeleteRes.body._id).should.equal(articleSaveRes.body._id);

                                // Call the assertion callback
                                done();
                            });
                    });
            });
    });

    it('should not be able to delete an article if not signed in', function(done) {
        // Set article user
        article.user = user;

        // Create new article model instance
        var articleObj = new Article(article);

        // Save the article
        articleObj.save(function() {
            // Try deleting article
            request(app).delete('/articles/' + articleObj._id)
                .expect(401)
                .end(function(articleDeleteErr, articleDeleteRes) {
                    // Set message assertion
                    (articleDeleteRes.body.message).should.match('User is not logged in');

                    // Handle article error error
                    done(articleDeleteErr);
                });

        });
    });

    afterEach(function(done) {
        User.remove().exec();
        Article.remove().exec();
        done();
    });
});
